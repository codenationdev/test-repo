// const sayHi = ()=>{
//        print('Hi 🌚') 
// }
import React from 'react'
import UserData from './UserData.js'
import DetailComponent from './components/DetailComponent.js'
import './styles.css'

class MainContent extends React.Component {
    constructor() {
        super()
        this.state = {
            userContact: UserData,
            Actual: 0,
        }
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(key) {
        this.setState({
            Actual: key,
        })
    }

    render() {
        // const UserComponent = this.state.userContact.map((item) => {
        //     return <UserItem item={item} key={item.id} />;
        // });
        const ContactName = this.state.userContact.map((item) => {
            return (
                <li key={item.id}>
                    <p
                        className="contact-name"
                        onClick={() => {
                            this.handleClick(item.id)
                        }}
                    >
                        {item.name}
                    </p>
                </li>
            )
        })

        return (
            <div className="main-container">
                <ul className="list-container">{ContactName}</ul>
                <DetailComponent
                    item={this.state.userContact[this.state.Actual]}
                />
            </div>
        )
    }
}

export default MainContent
